import UIKit

class ScoresController: UIViewController {

    @IBOutlet weak var backImageView: UIImageView!
    @IBOutlet weak var highScoresLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var gameScoreLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var scoresTableView: UITableView!
    
    private var scoresArray: [RaceResult] = StorageManager.shared.loadRaceResults()
    private var playerName: String = StorageManager.shared.loadPlayerName() ?? " "
    private var currentScore: Int = StorageManager.shared.loadScore() ?? 0
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scoresArray.sort(by:  {$0.score > $1.score})
        gameScoreLabel.text = """
        \("player".localized): \(playerName)
        \("result".localized): \(String(currentScore))
        """
    }
    
    override func viewDidLayoutSubviews() {
        self.backImageView.addParalaxEffect()
    }

    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popToRootViewController(animated: true)
    }
}


extension ScoresController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return scoresArray.count
    }
   
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ScoresTableViewCell", for: indexPath) as? ScoresTableViewCell else {
            return UITableViewCell()
        }
        cell.configure(with: scoresArray[indexPath.row])
 
        return cell
    }
     
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
}
