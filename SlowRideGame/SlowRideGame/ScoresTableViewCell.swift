
import UIKit

class ScoresTableViewCell: UITableViewCell {

    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var playerNameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    func configure(with object: RaceResult) {
        self.scoreLabel.text = String(object.score)
        self.playerNameLabel.text = object.playerName
        self.dateLabel.text = object.date
    }
    
    
    
}
