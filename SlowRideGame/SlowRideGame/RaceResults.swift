
import Foundation

class RaceResult: Codable {
    var playerName: String?
    var score: Int = 0
    var date: String

    init(playerName: String, score: Int = 0, date: String) {
        self.playerName = playerName
        self.score = score
        self.date = date
    }

    private enum CodingKeys: String, CodingKey {
        case playerName
        case score
        case date
    }


    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        playerName = try container.decode(String.self, forKey: .playerName)
        score = try container.decode(Int.self, forKey: .score)
        date = try container.decode(String.self, forKey: .date)
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try container.encode(self.playerName, forKey: .playerName)
        try container.encode(self.score, forKey: .score)
         try container.encode(self.date, forKey: .date)
    }

}
