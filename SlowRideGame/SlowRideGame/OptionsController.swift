
import UIKit

class OptionsController: UIViewController {
    
    @IBOutlet weak var optionsLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var playerNameLabel: UILabel!
    @IBOutlet weak var selectCarLabel: UILabel!
    @IBOutlet weak var selectBarriersLabel: UILabel!
    @IBOutlet weak var backImageView: UIImageView!
    @IBOutlet weak var playerNameTextField: UITextField!
    @IBOutlet weak var greenCarButton: UIButton!
    @IBOutlet weak var purpleCarButton: UIButton!
    @IBOutlet weak var blackCarButton: UIButton!
    @IBOutlet weak var firstBarrierButton: UIButton!
    @IBOutlet weak var secondBarrierButton: UIButton!
    @IBOutlet weak var thirdBarrierButton: UIButton!
    @IBOutlet weak var fourthBarrierButton: UIButton!
    @IBOutlet weak var saveNameButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configure()
        self.highlightSelectedCar()
        self.highligtSelectedBarrier()
    }
    
    override func viewDidLayoutSubviews() {
        self.backImageView.addParalaxEffect()
    }
    
    @IBAction func saveNameButtonPressed(_ sender: UIButton) {
        guard let playerName = playerNameTextField.text else { return }
        StorageManager.shared.savePlayerName(playerName: playerName)
    }
    
    @IBAction func greenCarButtonPressed(_ sender: UIButton) {
        let carImage = Constants.carImagesNamesArray[0]
       StorageManager.shared.saveCarImageName(carImageName: carImage)
        self.highlightSelectedCar()
    }
    
    @IBAction func purpleCarButtonPressed(_ sender: UIButton) {
        let carImage = Constants.carImagesNamesArray[1]
        StorageManager.shared.saveCarImageName(carImageName: carImage)
        self.highlightSelectedCar()
    }
    
    @IBAction func blackCarButtonPressed(_ sender: UIButton) {
        let carImage = Constants.carImagesNamesArray[2]
        StorageManager.shared.saveCarImageName(carImageName: carImage)
        self.highlightSelectedCar()
    }
    
    @IBAction func firstBarrierButtonPressed(_ sender: UIButton) {
        let barrierImageName = Constants.barriersImagesNamesArray[0]
        StorageManager.shared.saveBarrierImageName(barrierImageName: barrierImageName)
        self.highligtSelectedBarrier()
    }
    
    @IBAction func secondBarrierButtonPressed(_ sender: UIButton) {
        let barrierImageName = Constants.barriersImagesNamesArray[1]
        StorageManager.shared.saveBarrierImageName(barrierImageName: barrierImageName)
        self.highligtSelectedBarrier()
    }
    
    @IBAction func thirdBarrierButtonPressed(_ sender: UIButton) {
        let barrierImageName = Constants.barriersImagesNamesArray[2]
        StorageManager.shared.saveBarrierImageName(barrierImageName: barrierImageName)
        self.highligtSelectedBarrier()
    }
    
    @IBAction func fourthBarrierButtonPressed(_ sender: UIButton) {
        let barrierImageName = Constants.barriersImagesNamesArray[3]
        StorageManager.shared.saveBarrierImageName(barrierImageName: barrierImageName)
        self.highligtSelectedBarrier()
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func highlightSelectedCar() {
        let selectedCarName = StorageManager.shared.loadCarImageName()
        switch selectedCarName {
        case Constants.carImagesNamesArray[2]:
            self.blackCarButton.backgroundColor = .yellow
            self.greenCarButton.backgroundColor = .none
            self.purpleCarButton.backgroundColor = .none
        case Constants.carImagesNamesArray[1]:
            self.purpleCarButton.backgroundColor = .yellow
            self.greenCarButton.backgroundColor = .none
            self.blackCarButton.backgroundColor = .none
        default:
            self.greenCarButton.backgroundColor = .yellow
            self.blackCarButton.backgroundColor = .none
            self.purpleCarButton.backgroundColor = .none
        }
    }
    
    func highligtSelectedBarrier() {
        let selectedBarrierName = StorageManager.shared.loadBarrierImageName()
        switch selectedBarrierName {
        case Constants.barriersImagesNamesArray[3]:
            self.fourthBarrierButton.backgroundColor = .yellow
            self.thirdBarrierButton.backgroundColor = .none
            self.secondBarrierButton.backgroundColor = .none
            self.firstBarrierButton.backgroundColor = .none
        case Constants.barriersImagesNamesArray[2]:
            self.fourthBarrierButton.backgroundColor = .none
            self.thirdBarrierButton.backgroundColor = .yellow
            self.secondBarrierButton.backgroundColor = .none
            self.firstBarrierButton.backgroundColor = .none
        case Constants.barriersImagesNamesArray[1]:
            self.fourthBarrierButton.backgroundColor = .none
            self.thirdBarrierButton.backgroundColor = .none
            self.secondBarrierButton.backgroundColor = .yellow
            self.firstBarrierButton.backgroundColor = .none
        default:
            self.fourthBarrierButton.backgroundColor = .none
            self.thirdBarrierButton.backgroundColor = .none
            self.secondBarrierButton.backgroundColor = .none
            self.firstBarrierButton.backgroundColor = .yellow
        }
    }
    
    func configure() {
        self.playerNameTextField.text = StorageManager.shared.loadPlayerName()
        self.playerNameLabel.textColor = .black
        self.selectCarLabel.textColor = .black
        self.selectBarriersLabel.textColor = .black
        self.containerView.backgroundColor = .white
    }

    
}


