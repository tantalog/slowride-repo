
import Foundation

import UIKit
class Constants {
    static let radius:CGFloat = 10
    static let carImagesNamesArray: [String] = ["carGreen", "carPurple", "carBlack"]
    static   let treesImagesArray: [UIImage?] = [ UIImage(named: "firstTree"), UIImage(named: "secondTree"), UIImage(named: "thirdTree")]
    static let barriersImagesNamesArray: [String] = ["firstBarrier", "secondBarrier", "thirdBarrier", "fourthBarrier"]
    static let playerCarsize: CGSize = CGSize(width: 60, height: 100)
    static let coinSize: CGSize = CGSize(width: 40, height: 40)
    static let barrierSize: CGSize = CGSize(width: 100, height: 100)
    static let trafficTimeInterval = 0.5
    static let eventChekTimeInterval = 0.05
}
