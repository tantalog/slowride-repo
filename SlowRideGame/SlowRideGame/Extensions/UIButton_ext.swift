
import Foundation
import UIKit

extension UIButton {

    func roundCorners () {
        self.layer.cornerRadius = self.frame.height/2
    }

    func dropShadow() {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.5
        layer.shadowOffset = CGSize(width: 0, height: 0)
        layer.shadowRadius = 10

        layer.shadowPath = UIBezierPath(rect: bounds).cgPath

        layer.shouldRasterize = true
    }

    func addGradientWithRadius(_ radius: CGFloat) {
        let gradient = CAGradientLayer()

        gradient.colors = [UIColor.red.cgColor, UIColor.yellow.cgColor]
        gradient.opacity = 0.6
        gradient.startPoint = CGPoint(x: 0.0, y: 0.0)
        gradient.endPoint = CGPoint(x: 1.0, y: 1.0)
        gradient.cornerRadius = radius
        gradient.frame = self.bounds
        self.layer.addSublayer(gradient)
    }

    func dropShadowWithRadius(_ radius: CGFloat) {
        self.layer.cornerRadius = radius
        self.layer.shadowColor = UIColor.darkGray.cgColor
        self.layer.shadowOffset = CGSize(width: 5, height: 5)
        self.layer.shadowRadius = 1
        self.layer.shadowOpacity = 1
        self.layer.masksToBounds = false

    }
}
