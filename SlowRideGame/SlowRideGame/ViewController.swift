import UIKit
import AVFoundation

class ViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var optionsButton: UIButton!
    @IBOutlet weak var scoresButton: UIButton!
    
    private var audioPlayer: AVAudioPlayer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureButtons()
    }
    
    
    @IBAction func playMusicButtonPressed(_ sender: UIButton) {
        self.playAudio()
    }
    
    @IBAction func stopMusicButtonPressed(_ sender: UIButton) {
        self.stopAudio()
    }
 
    
    @IBAction func startButtonPressed(_ sender: UIButton) {
        guard let gameController = self.storyboard?.instantiateViewController(withIdentifier: "GameController") as? GameController else {
            return
        }
        self.navigationController?.pushViewController(gameController, animated: true)
    }
    
    @IBAction func optionsButtonPressed(_ sender: UIButton) {
        guard let optionsController = self.storyboard?.instantiateViewController(withIdentifier: "OptionsController") as? OptionsController else {
            return
        }
        self.navigationController?.pushViewController(optionsController, animated: true)
    }
    
    @IBAction func scoresButtonPressed(_ sender: UIButton) {
        guard let scoresController = self.storyboard?.instantiateViewController(withIdentifier: "ScoresController") as? ScoresController else {
            return
        }
        self.navigationController?.pushViewController(scoresController, animated: true)
    }
    
    func configureButtons() {
        self.startButton.addGradientWithRadius(Constants.radius)
        self.startButton.dropShadowWithRadius(Constants.radius)
        self.optionsButton.addGradientWithRadius(Constants.radius)
        self.optionsButton.dropShadowWithRadius(Constants.radius)
        self.scoresButton.addGradientWithRadius(Constants.radius)
        self.scoresButton.dropShadowWithRadius(Constants.radius)
    }
    
    func playAudio() {
            let sound = URL(fileURLWithPath: Bundle.main.path(forResource: "SlowRide", ofType: "mp3")!)
            
            try! AVAudioSession.sharedInstance().setCategory(AVAudioSession.Category.playback)
            try! AVAudioSession.sharedInstance().setActive(true)
            
            try! audioPlayer = AVAudioPlayer(contentsOf: sound)
            audioPlayer!.prepareToPlay()
            audioPlayer!.play()
        }
    
    func stopAudio() {
        if let player = self.audioPlayer {
            player.stop()
        }
    }
    
    
}

