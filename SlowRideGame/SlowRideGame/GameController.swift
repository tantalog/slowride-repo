import UIKit
//import CoreMotion

class GameController: UIViewController {
    
    @IBOutlet weak var roadView: UIView!
    @IBOutlet weak var asfaltImageView: UIImageView!
    @IBOutlet weak var rightSandImageView: UIImageView!
    @IBOutlet weak var leftSandImageView: UIImageView!
    @IBOutlet weak var playerNameLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    
    let playerCar = UIImageView()
    var barrier = UIImageView()
    var coin = UIImageView()
    var coinCounter: Int = 0
    var date: String = ""
    var carJumps = false
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.createSwipeRecognizer()
        self.createTapRecognizer()
        self.createTimers()
        self.loadSettings()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.createPlayerCar()
    }
    
    
    @IBAction func rightSwipeDetected(_ recognizer: UISwipeGestureRecognizer) {
        UIView.animate(withDuration: 0.3) {
            self.playerCar.frame.origin.x += self.playerCar.frame.width
        }
    }
    
    @IBAction func leftSwipeDetected(_ recognizer: UISwipeGestureRecognizer) {
        UIView.animate(withDuration: 0.3) {
            self.playerCar.frame.origin.x -= self.playerCar.frame.width
        }
    }
    
    @IBAction func tapDetected(_ recognizer: UITapGestureRecognizer) {
        UIView.animate(withDuration: 0.3) {
            
            self.carJumps = true
            self.playerCar.frame.size.height *= 2
            self.playerCar.frame.size.width *= 2
        } completion: { (Bool) in UIView.animate(withDuration: 0.3) {
            self.playerCar.frame.size.height /= 2
            self.playerCar.frame.size.width /= 2
            
        } completion: { (Bool) in
            self.carJumps = false
        }
        }
    }

    
    func createPlayerCar() {
        let playerCarSize = Constants.playerCarsize
        let startPosition = (x: self.roadView.frame.width - Constants.playerCarsize.width - 5, y: self.roadView.frame.height - Constants.playerCarsize.height - 25)
        
        self.playerCar.image = UIImage(named: StorageManager.shared.loadCarImageName() ?? Constants.carImagesNamesArray[0])
        self.playerCar.frame = CGRect(x: startPosition.x, y: startPosition.y, width: playerCarSize.width, height: playerCarSize.height)
        self.roadView.addSubview(self.playerCar)
    }
    
    func loadSettings() {
        self.playerNameLabel.text = StorageManager.shared.loadPlayerName()
        self.barrier.image = UIImage(named: StorageManager.shared.loadBarrierImageName() ?? Constants.barriersImagesNamesArray[0])
        self.date = self.dateToString()
        self.scoreLabel.text = " \("Score".localized): " + "\(self.coinCounter)"
    }
    
    func createSwipeRecognizer() {
        let rightSwipeRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(rightSwipeDetected))
        rightSwipeRecognizer.direction = .right
        self.view.addGestureRecognizer(rightSwipeRecognizer)
        let leftSwipeRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(leftSwipeDetected))
        leftSwipeRecognizer.direction = .left
        self.view.addGestureRecognizer(leftSwipeRecognizer)
    }
    
    func createTapRecognizer() {
        let tapRecognizer = UITapGestureRecognizer(target: self, action: #selector(tapDetected))
        tapRecognizer.numberOfTapsRequired = 1
        self.view.addGestureRecognizer(tapRecognizer)
    }
    
    func createTimers() {
        Timer.scheduledTimer(timeInterval: Constants.trafficTimeInterval, target: self, selector: #selector(createDividingStrip), userInfo: nil, repeats: true)
        Timer.scheduledTimer(timeInterval: Constants.trafficTimeInterval, target: self, selector: #selector(createMotionRight), userInfo: nil, repeats: true)
        Timer.scheduledTimer(timeInterval: Constants.trafficTimeInterval, target: self, selector: #selector(createMotionLeft), userInfo: nil, repeats: true)
        Timer.scheduledTimer(timeInterval: Double.random(in: 3...4), target: self, selector: #selector(createBarriers), userInfo: nil, repeats: true)
        Timer.scheduledTimer(timeInterval: Double.random(in: 3...4), target: self, selector: #selector(createCoins), userInfo: nil, repeats: true)
        Timer.scheduledTimer(timeInterval: Constants.eventChekTimeInterval, target: self, selector: #selector(carsDidCrushed), userInfo: nil, repeats: true)
        Timer.scheduledTimer(timeInterval: Constants.eventChekTimeInterval, target: self, selector: #selector(coinCollected), userInfo: nil, repeats: true)
        Timer.scheduledTimer(timeInterval: Constants.eventChekTimeInterval, target: self, selector: #selector(carFalloutLeft), userInfo: nil, repeats: true)
        Timer.scheduledTimer(timeInterval: Constants.eventChekTimeInterval, target: self, selector: #selector(carFalloutRight), userInfo: nil, repeats: true)
    }
    
    @objc func createDividingStrip() {
        let dividingStrip = UIView()
        dividingStrip.frame = CGRect(x: self.asfaltImageView.frame.width/2 - dividingStrip.frame.width/2, y: -400, width: 10, height: 250)
        dividingStrip.backgroundColor = .white
        self.asfaltImageView.addSubview(dividingStrip)
        UIView.animate(withDuration: 3, animations: {
            dividingStrip.frame.origin.y += self.asfaltImageView.frame.height + 600
        }) { (_) in
            dividingStrip.removeFromSuperview()
        }
    }
    
    @objc func createMotionRight () {
        let rightObject = UIImageView()
        rightObject.frame = CGRect(x: 5, y: -150, width: 60, height: 60)
        rightObject.image = Constants.treesImagesArray[Int.random(in: 0...Constants.treesImagesArray.count-1)]
        self.rightSandImageView.addSubview(rightObject)
        
        UIView.animate(withDuration: 3, animations: {
            rightObject.frame.origin.y += self.rightSandImageView.frame.height + 300
        }) { (_) in
            rightObject.removeFromSuperview()
        }
    }
    
    @objc func createMotionLeft () {
        let leftObject = UIImageView()
        leftObject.frame = CGRect(x: 10, y: -150, width: 60, height: 60)
        leftObject.image = Constants.treesImagesArray[Int.random(in: 0...Constants.treesImagesArray.count-1)]
        self.leftSandImageView.addSubview(leftObject)
        
        UIView.animate(withDuration: 3, animations: {
            leftObject.frame.origin.y += self.leftSandImageView.frame.height + 300
        }) { (_) in
            leftObject.removeFromSuperview()
        }
    }
    
    @objc func  createBarriers() {
        barrier.frame = CGRect(x: CGFloat.random(in: 5 ... (self.roadView.frame.width - self.playerCar.frame.width)), y: -200, width: Constants.barrierSize.width, height: Constants.barrierSize.height)
        barrier.image = UIImage(named: StorageManager.shared.loadBarrierImageName() ?? Constants.barriersImagesNamesArray[0])
        self.roadView.addSubview(self.barrier)
        UIView.animate(withDuration: 3, animations: {
            self.barrier.frame.origin.y += self.roadView.frame.height + 500
        }) { (_) in
            self.barrier.removeFromSuperview()
        }
    }
    
    @objc func createCoins() {
        coin.frame = CGRect(x: CGFloat.random(in: 5 ... (self.roadView.frame.width - self.playerCar.frame.width)), y: -200, width: Constants.coinSize.width, height: Constants.coinSize.height)
        coin.image = UIImage(named: "coin")
        self.roadView.addSubview(self.coin)
        UIView.animate(withDuration: 3, animations: {
            self.coin.frame.origin.y += self.roadView.frame.height + 500
        }) { (_) in
            self.coin.removeFromSuperview()
        }
    }
    
    @objc func carsDidCrushed () {
        guard let userCarPosition = self.playerCar.layer.presentation()?.frame else {
            return
        }
        guard let barrierPosition = self.barrier.layer.presentation()?.frame else {
            return
        }
        if !self.carJumps, userCarPosition.intersects(barrierPosition) {
            self.saveRaceResult()
            self.endGame()
        }
    }
    
    @objc func coinCollected () {
        guard let userCarPosition = self.playerCar.layer.presentation()?.frame else {
            return
        }
        guard let coinPosition = self.coin.layer.presentation()?.frame else {
            return
        }
        if userCarPosition.intersects(coinPosition) {
            coinCounter += 1
            scoreLabel.text = " Score: \(self.coinCounter)"
            self.coin.removeFromSuperview()
        }
    }
    
    @objc func carFalloutLeft() {
        if !self.carJumps, self.playerCar.frame.origin.x < 0 - self.playerCar.frame.width/2 {
            self.playerCar.frame.origin.x += self.playerCar.frame.width
            saveRaceResult()
            endGame()
        }
    }
    
    @objc func carFalloutRight() {
        if !self.carJumps, self.playerCar.frame.origin.x > self.roadView.frame.width - self.playerCar.frame.width {
            self.playerCar.frame.origin.x -= self.playerCar.frame.width
            saveRaceResult()
            endGame()
        }
    }
    
    func endGame() {
        self.barrier.layer.removeAllAnimations()
        guard let scoresController = self.storyboard?.instantiateViewController(withIdentifier: "ScoresController") as? ScoresController else {
            return
        }
        self.navigationController?.pushViewController(scoresController, animated: true)
        self.view.removeFromSuperview()
    }
    
    func saveRaceResult() {
        let playerName = StorageManager.shared.loadPlayerName() ?? ""
        let score = self.coinCounter
        let date = self.date
        let raceResult = RaceResult(playerName: playerName, score: score, date: date)
        StorageManager.shared.saveRaceResult(raceResult: raceResult)
        StorageManager.shared.saveScore(score: score)
    }
    
    func dateToString() -> String {
        let formatterString = DateFormatter()
        formatterString.dateFormat = "dd MMM yyyy, HH:MM a"
        let date = Date()
        let stringDate = formatterString.string(from: date)
        return stringDate
    }
    
}
