import Foundation
import UIKit

class StorageManager {
    
    enum Keys: String {
        
        case playerName = "playerName"
        case carImageName = "carImageName"
        case carImage = "carImage"
        case barrierImageName = "barrierImageName"
        case score = "score"
        case raceResults = "raceResults"
    }
    
    
    static let shared = StorageManager()
    
    private init() {}
    private let defaults = UserDefaults.standard
    
    func saveRaceResult(raceResult: RaceResult) {
        var resultsArray = StorageManager.shared.loadRaceResults()
        resultsArray.append(raceResult)
        defaults.set(encodable: resultsArray, forKey: Keys.raceResults.rawValue)
    }
    
    func loadRaceResults() -> [RaceResult] {
        let resultsArray = defaults.value([RaceResult].self, forKey: Keys.raceResults.rawValue)
        if let resultsArray = resultsArray {
            return resultsArray
        } else  {
            return []
        }
    }
    
    
    func savePlayerName(playerName: String) {
        UserDefaults.standard.set(playerName, forKey: Keys.playerName.rawValue)
    }
    func saveScore(score: Int) {
        UserDefaults.standard.set(score, forKey: Keys.score.rawValue)
    }
    
    func saveCarImageName(carImageName: String) {
        UserDefaults.standard.set(carImageName, forKey: Keys.carImageName.rawValue)
    }
    
    func saveBarrierImageName(barrierImageName: String) {
        UserDefaults.standard.set(barrierImageName, forKey: Keys.barrierImageName.rawValue)
    }
    
    func loadPlayerName() -> String? {
        guard let playerName = UserDefaults.standard.value(forKey: Keys.playerName.rawValue) as? String else { return nil }
        
        return playerName
    }
    func loadScore() -> Int? {
        guard let score = UserDefaults.standard.value(forKey: Keys.score.rawValue) as? Int else { return nil }
        
        return score
    }
    
    func loadCarImageName() -> String? {
        guard let carImageName = UserDefaults.standard.value(forKey: Keys.carImageName.rawValue) as? String else { return nil }
        
        return carImageName
    }
    
    func loadBarrierImageName() -> String? {
        guard let barrierImageName = UserDefaults.standard.value(forKey: Keys.barrierImageName.rawValue) as? String else { return nil }
        
        return barrierImageName
    }
    
    func incrementStartCount() {
        guard let count = UserDefaults.standard.value(forKey: "count") as? Int else { return }
        UserDefaults.standard.set(count+1, forKey: "count")
    }
    
    
    
    
}
